var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync');
var connect = require('gulp-connect-php');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');

// var imagemin = require('gulp-imagemin');
// var tingpng = require('gulp-tinypng');
var runSequence = require('run-sequence');



gulp.task('sass', function () {
	
	return gulp.src(["sass/main.scss"])
		.pipe(plumber())
		.pipe(sass.sync())
		.pipe(autoprefixer({
			browsers: ['last 5 versions', 'IE 9'],
			cascade: false
		}))
		.pipe(sourcemaps.init()).pipe(sourcemaps.write('/'))
		.pipe(gulp.dest("css"))
		.pipe(browserSync.stream());

});


gulp.task('cssnano', function() {
    
    return gulp.src('css/*.css')
        .pipe(cssnano())
        .pipe(gulp.dest('css/'));

});


gulp.task('serve', function(){

	connect.server({}, function (){
		browserSync({
			proxy: '127.0.0.1:8000',
			port: 8080
		});
	});

});


gulp.task('clean', function(){
	
	return del('_build/');

});


gulp.task('copy', function(){
	gulp.src([
			"!**/node_modules/**", "!**/node_modules/", "!**/.gitignore", "!**/bower.json",	"!**/composer.json", "!**/composer.lock", 
			"!**/config.json", "!**/gulpfile.js", "!**/package.json",	"!**/package-lock.json", "!**/*.scss", "!**/sass", "**/**"
		])
		.pipe(gulp.dest('_build/'));
});


gulp.task('scripts', function() {
	
	return gulp.src(['js/main.js'])
		.pipe(concat('main.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('js'));

});


gulp.task('watch', function () {
	
	gulp.watch(['sass/**/*.scss'], ['sass']);
	gulp.watch(['js/*.js'], ['scripts']);
	gulp.watch(['**/*.html', '**/*.php', 'js/*.js'], browserSync.reload);

});


gulp.task('default', ['watch', 'serve']);


gulp.task('build', function(){

    runSequence('sass', 'cssnano', 'copy')

});